<?php

class Compval extends CI_Model{
    const TABLE = "CompVal";
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function getPersonsInBuilding(){
        /* @var $res CI_DB_mysql_result */
        $res = $this->db->select()->from(self::TABLE)->where("Name", "total")->get();
        
        if ($res->num_rows() > 0) {
            return $res->first_row()->Value;
        }
        else return 0;
    }
    
    public function setPersonsInBuilding($persons){
        $this->db->set("Name", "total");
        $this->db->set("Value", $persons, true);
        $this->db->replace(self::TABLE);
    }
    
    public function getNextAction(){
         /* @var $res CI_DB_mysql_result */
        $res = $this->db->select()->from(self::TABLE)->where("Name", "action")->get();
        
        if ($res->num_rows() > 0) {
            return $res->first_row()->Value;
        }
        else return false;
    }
    
    public function setNextAction($action){
        $this->db->set("Name", "action");
        $this->db->set("Value", $action, true);
        $this->db->replace(self::TABLE);
    }
    
    public function clearNextAction(){
        
        $this->db->query("DELETE FROM " . self::TABLE . " WHERE Name = 'action'");
        /*$this->db->where("Name", "action");
        $this->db->delete(self::TABLE);*/
    }
}