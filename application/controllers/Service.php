<?php

class Service extends CI_Controller {
    const SLEEP = 2;
    const SERIAL_DEVICE = '/dev/ttyACM0';
    protected $serial;
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('phpserial');
        
        $this->load->model("compval");
        
        $this->serial = new PhpSerial();
        $this->serial->deviceSet(self::SERIAL_DEVICE);       // Got from old code
        $this->serial->confStopBits(1);
        $this->serial->confParity('none');
        $this->serial->confFlowControl('none');
        $this->serial->confBaudRate(9600);
        $this->serial->autoFlush = true;
        
        $this->serial->deviceOpen();
        sleep(2); // Sleep two seconds, waiting arduino to restart (hardware hack can avoid arduino restart) on new connection
        
        $this->serial->readPort(); // Try to clean the buffer
    }
    
    public function index(){
        while (true){
            echo "Requesting data From Arduino\n";
            $this->serial->sendMessage("GET\r\n");
            //$this->serial->serialflush();
            $data = $this->serial->readPort();
            $this->serial->sendMessage("ACK\r\n");
            $this->cleanResult($data);
            $this->processArduinoResponse($data);
            echo "---------------------------------\n";
            
            $action = $this->compval->getNextAction();
            if ($action){
                $this->serial->sendMessage($action . "\r\n");
                $this->compval->clearNextAction();
            }
            
            sleep(self::SLEEP);
        }
    }
    
    /**
     * Cleans the result, and if its not an array splits it by each line
     */
    protected function cleanResult(&$result){
        if (!is_array($result)){
            $result = explode("\n",$result);
        }
        
        foreach ($result as $key => &$value) {
            $value = trim($value);
            
            if (strlen($value) == 0){
                unset($result[$key]);
            }
        }
    }
    
    public function processArduinoResponse(&$response){
        $persons = $this->compval->getPersonsInBuilding();
        $orig_persons = $persons;
        
        $prev_value = "";
        foreach ($response as $value){
            if (is_numeric($value)) {
                switch ($prev_value){
                    case "IN":
                        $persons += intval($value);
                        break;

                    case "OUT":
                        $persons -= intval($value);
                        break 2;
                }
            }
            else {
                $prev_value = $value;
            }
        }
        
        if ($orig_persons == $persons) {
            echo "Nothing to save, computed values are the same\n";
        }
        else {
            echo "------------------------------\n";
            echo "Saving persons in building $persons\n";
            echo "------------------------------\n";
            $this->compval->setPersonsInBuilding($persons);
        }
        
    }
    
    public function __destruct() {
        $this->serial->deviceClose();
    }
}
