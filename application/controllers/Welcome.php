<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("compval");
    }

    public function index() {
        header( "refresh:3;url=/" );
        $visitors = $this->compval->getPersonsInBuilding();    
        $this->load->view('welcome_message', array('visitors' => $visitors));
    }

	public function resetCounter() {
		$this->compval->setPersonsInBuilding(0);
		$this->index();		
	}
	
	public function alarm() {
		$this->compval->setNextAction("ALA");
		$this->index();		
	}
}
