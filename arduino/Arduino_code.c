#define BUFFER 4          // The buffer length
#define SENSOR_DELAY 800  // The delay since a whole sense is triggered in ms

int count_in = 0, 				// Keep track of current persons going in
count_out = 0;				    // Keep track of current persons going out

int inval, outval;				// Sensor data

byte last_triggered = 0;	// Will hold which direction sensor was triggered
// until the person is out, or the time span has gone
// 0 means not triggered before
// 1 means the previous sensor was in
// 2 means the previous sensor was out

unsigned long last_time = 0;	// Time returned from millis() [this will overflow
// after 50 days running

const unsigned long max_time_detection = 1000;	// This is the maximum time that
// can pass between the first and the second 
// sensor are triggered to detect the direction
// of the moving object

char buffer[BUFFER];          // To store the incoming data from php

int flag = 0; // If flag is 0, buzzer is currently off. If flag is 1, buzzer is currently on.

void setup() {

  pinMode(A0, INPUT);
  pinMode(A5, INPUT);
  pinMode(10, OUTPUT); // Buzzer
  Serial.begin(9600);
  delay(2000);
  //  Serial.println();

}


void loop() {  
  if (Serial.available()){    // If data available to read on the serial line    
    if (readline(Serial.read(), buffer, BUFFER)){  // Once we get a whole line, process the request
      processRequestOnBuffer();
    }
  }

  // Read the sensor values
  inval = analogRead(A0);
  outval = analogRead(A5);

  if (last_time - millis() > 1000) {		// If the time span has gone, reset 
    last_triggered = 0;					        // the last triggered to none
  }

  if(inval < 300) { 
    if (last_triggered == 0){			// If nothin was triggered before, we 
      last_triggered = 1;				  // are taking someone in, lets recor
      last_time = millis();			  // it somewhere and save when did this happened
    }
    else {
      if (last_triggered == 2){		// If the out sensor was triggered first
        last_triggered = 0;			  // reset last trigger and someone went out
        count_out ++;
        delay(SENSOR_DELAY);
      }
    }
  }

  if(outval < 300) {
    if (last_triggered == 0) {			// If nothing was triggered we are
      last_triggered = 2;				    // taking someone out, so lets store
      last_time = millis();			    // what and when was triggered
    }
    else {
      if (last_triggered == 1){		// If the in sensor was triggered first
        last_triggered = 0;			  // reset the trigger
        count_in ++;              // count someone in
        delay (SENSOR_DELAY);
      }
    }
  }
}

void processRequestOnBuffer(){
  if (strcmp(buffer, "GET") == 0){
    sendData();
  }
  if (strcmp(buffer, "ACK") == 0){
    count_in = 0;
    count_out = 0;
  }
  if (strcmp(buffer, "ALA") == 0){
    // Checking whether buzzer is on or not. Then triggering it accordingly
    if (flag == 0) {
      digitalWrite(10,HIGH);
      flag = 1;
    }
    if (flag == 1) {
      digitalWrite(10,LOW);
      flag = 0;
    }
  }
}
void sendData() {
  Serial.println("IN ");
  Serial.println(count_in);
  Serial.println("OUT");
  Serial.println(count_out);
}


/**
 * Code obtained from: http://hacking.majenko.co.uk/reading-serial-on-the-arduino
 **/
bool readline(byte readch, char *buffer, int len)
{
  static int pos = 0;
  int rpos;

  if (readch > 0) {
    switch (readch) {
    case '\n': // Ignore new-lines
      break;
    case '\r': // Return on CR
      rpos = pos;
      pos = 0;  // Reset position index ready for next time
      return true;
    default:
      if (pos < len-1) {
        buffer[pos++] = readch;
        buffer[pos] = '\0';
      }
    }
    // No end of line has been found, so return -1.
  }
  return false;

}



